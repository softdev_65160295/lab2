/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author natta
 */
public class Lab2 {

    static char[][] Table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;
    static boolean contiune = true;

    public static void main(String[] args) {
        printWelcome();
        while (contiune) {
            clearTable();
            while (true) {
                printTable();
                printTurn();
                inputRowCol();
                if (isWin()) {
                    printTable();
                    printWinner();
                    break;
                } else if (isDraw()) {
                    printTable();
                    printDraw();
                    break;
                }
                changePlayer();
            }
            inputContinue();
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    private static void printTable() {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(Table[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.println("Please input row col");
            row = kb.nextInt();
            col = kb.nextInt();
            if (row > 3 || row < 1 || col < 1 || col > 3) {
                System.out.println("Number out of bound");
            } else if (Table[row - 1][col - 1] == ('-')) {
                Table[row - 1][col - 1] = currentPlayer;
                return;
            } else {
                System.out.println("The position has already taken.");
            }
        }
    }

    private static void changePlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static void printWinner() {
        System.out.print(currentPlayer);
        System.out.println(" win");

    }

    private static boolean isWin() {
        if (checkRow()) {
            return true;
        }
        if (checkCol()) {
            return true;
        }
        if (checkX1()) {
            return true;
        }
        if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (Table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (Table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (Table[0][0] == currentPlayer && Table[1][1] == currentPlayer && Table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (Table[0][2] == currentPlayer && Table[1][1] == currentPlayer && Table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        int Turn = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Table[i][j] != '-') {
                    Turn++;
                }
            }
        }
        if (Turn == 9) {
            return true;
        }
        return false;
    }

    private static void printDraw() {
        System.out.println("Draw");
    }

 

    private static void inputContinue() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Continue ?");
        System.out.println("Y / N");
        String Anw = kb.next();
        if (Anw.equals("Y")) {

        }
        if (Anw.equals("N")) {
            contiune = !contiune;
        }
    }

    private static void clearTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Table[i][j] = '-';
            }
        }
    }
}
